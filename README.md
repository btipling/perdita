# peridita

Generates SVGs from email addresses. Meant to be an avatar generator. It's not very good yet, it's a work in progress.

Current status: in development.


## Example Usage:
```
yarn node .\build\main.js generate --destination ./out --emails  "peridita@disney.com" "pongo@disney.com"
```

### Help output:

```
> yarn node .\build\main.js --help
Usage: main [options] [command]

Options:
  -V, --version       output the version number
  -h, --help          display help for command

Commands:
  generate [options]  generate avatars for a bunch of emails
  help [command]      display help for command
```

```
> yarn node .\build\main.js generate --help
Usage: main generate [options]

generate avatars for a bunch of emails

Options:
  -d, --destination <destination>
  -s, --salt <salt>                The salt to use to generate the avatar (default: "perrdita")
  -e, --emails [emails...]
  -h, --help                       display help for command
```

## Example output:

![bjorn.tipling.com.svg](https://gitlab.com/btipling/perdita/uploads/e0562c675366db118a01d976ee974171/bjorn.tipling.com.svg)
&nbsp;&nbsp;&nbsp;
![bjorn.tipling.gmail.com.svg](https://gitlab.com/btipling/perdita/uploads/8138d22bb29783883cebaf5e0ef9461f/bjorn.tipling.gmail.com.svg)
&nbsp;&nbsp;&nbsp;
![max.imgur.com.svg](https://gitlab.com/btipling/perdita/uploads/21780322fe6b9692102b6fe59c3c6c39/max.imgur.com.svg)
&nbsp;&nbsp;&nbsp;
![tom.myspace.com.svg](https://gitlab.com/btipling/perdita/uploads/7bce3c261c4b565a48f27d50512a19da/tom.myspace.com.svg)
&nbsp;&nbsp;&nbsp;
![walter.blizzard.com.svg](https://gitlab.com/btipling/perdita/uploads/74a46f9f941c463b225538f212fc6874/walter.blizzard.com.svg)

