
## v4 stable hash
![bjorn.tipling.com.svg](/uploads/e0562c675366db118a01d976ee974171/bjorn.tipling.com.svg)
&nbsp;&nbsp;&nbsp;
![bjorn.tipling.gmail.com.svg](/uploads/8138d22bb29783883cebaf5e0ef9461f/bjorn.tipling.gmail.com.svg)
&nbsp;&nbsp;&nbsp;
![max.imgur.com.svg](/uploads/21780322fe6b9692102b6fe59c3c6c39/max.imgur.com.svg)
&nbsp;&nbsp;&nbsp;
![tom.myspace.com.svg](/uploads/7bce3c261c4b565a48f27d50512a19da/tom.myspace.com.svg)
&nbsp;&nbsp;&nbsp;
![walter.blizzard.com.svg](/uploads/74a46f9f941c463b225538f212fc6874/walter.blizzard.com.svg)


## v3 added borders:

![max.svg](/uploads/f0655d191dbae3853d4e947e305e116c/max.svg)
&nbsp;&nbsp;&nbsp;
![tom.svg](/uploads/a778095da71445a1f21e0ee5ecf0edf3/tom.svg)
&nbsp;&nbsp;&nbsp;
![bjorn.svg](/uploads/1c69eb95463b45706f43de6f64e19991/bjorn.svg)

## v2

![tom.svg](/uploads/a292a7eede46c98ddc1d7341ddeced82/tom.svg)
&nbsp;&nbsp;&nbsp;
![bjorn.svg](/uploads/26ebe6d5e4cb89cb74b69ab3c37b4a9f/bjorn.svg)
&nbsp;&nbsp;&nbsp;
![bjorn.svg](/uploads/8e2270fd02c8bfe25e9675170a7fa33f/bjorn.svg)
&nbsp;&nbsp;&nbsp;
![max.svg](/uploads/3b458f4dde97c9bcbe606ffb8dda9d5c/max.svg)
&nbsp;&nbsp;&nbsp;
![tom.svg](/uploads/cfefd07e7c3ee4d2fec7cbdc09b484c3/tom.svg)
&nbsp;&nbsp;&nbsp;
![max.svg](/uploads/3a4c4c6cf6fa9435fd8a75cdbe684655/max.svg)

## v1

![bjorn.svg](/uploads/392e5062e0c1ec92643947859066f7b3/bjorn.svg)
&nbsp;&nbsp;&nbsp;
![max.svg](/uploads/e977fe89bdda4cda229960b0e780cff5/max.svg)
&nbsp;&nbsp;&nbsp;
![tom.svg](/uploads/dfec8ff71f586024408b72d8e0555cdb/tom.svg)
