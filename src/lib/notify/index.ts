import { Pipeline } from '../pipeline'

export const notifyOfInvalidEmail = (badEmail: string) => { console.log(`'${badEmail}' is an invalid email and was ignored.`) }
export const notifyOfBuildStep = (arg: Pipeline) => console.log(`Building avatar for ${arg.ident.user} at ${arg.ident.domain} to ${arg.destination}`)
