import fs from 'fs'

export const writer = async (svg: string, destination: string): Promise<void> => {
    fs.writeFile(destination, Buffer.from(svg), (err) => {
        if (err) {
            console.log('error writing svg')
        }
    })
}
