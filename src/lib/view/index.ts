import fs from 'fs'
import mustache from 'mustache'

type Pixel = {
    x: number,
    y: number,
    fill: string,
}

type toN = (i: number) => number

const nCornerColumns = 3
const nCornerRows = 3
const nBorderColumns = 1
const nBorderRows = 20
const nPortaitColumns = 5
const nPortraitRows = 5

const borderOffset = 1
const portraitOffset = 0.5

const newPixel = (x: number, y: number, fill: string): Pixel => ({ x, y, fill })
const blank = '#FFFFFF'

const iToX = (numColumns: number) => (i: number): number => i % numColumns
const iToY = (numColumns: number, numRows: number) => (i: number): number => Math.floor(i / numColumns) % numRows
const toPixel = (toX: toN, toY: toN) => (fill: string) => (x: number, i: number) => x ? newPixel(toX(i), toY(i), fill) : newPixel(toX(i), toY(i), blank)
const offSet = (offset: number): toN => (i: number) => i + offset
const fillPixel = (fill: string, x: number, y: number): Pixel => ({ fill, x, y })
const addCorner = (pixels: Pixel[], fill: string): void => {
    for (let i = 0; i < nCornerColumns * nCornerRows; i++) pixels.push(fillPixel(fill, iToX(nCornerColumns)(i), iToY(nCornerColumns, nCornerRows)(i)))
}
const templateGetter = (): () => Promise<string> => {
    let template = ""
    return async (): Promise<string> => {
        if (template.length > 0) {
            console.log('had template')
            return template
        }
        console.log('getting template')
        template = await ((): Promise<string> => new Promise((resolve, error) => {
            fs.readFile(`${__dirname}/view.mustache`, (err, data) => {
                if (err) {
                    error(err)
                    return
                }
                resolve(data.toString())
            })
        }))()
        return template
    }
}
const getTemplate = templateGetter()

export const pixelMapToSVG = async (borderPixelMap: number[], portraitPixelMap: number[], borderFill: string, portraitFill: string): Promise<string> => {
    console.log('pixelMapToSVG')
    const template = await getTemplate()
    const borderPixels: Pixel[] = borderPixelMap
        .map(toPixel(iToX(nBorderColumns), iToY(nBorderColumns, nBorderRows))(borderFill))
        .map((p: Pixel) => {
            p.x = offSet(borderOffset)(p.x)
            p.y = offSet(borderOffset + 2)(p.y)
            return p
        })
    for (let i = 0; i < nBorderRows; i++) borderPixels.push(fillPixel(borderFill, 0, i + 2))
    for (let i = 0; i < nBorderRows; i++) borderPixels.push(fillPixel(borderFill, 2, i + 2))
    addCorner(borderPixels, borderFill)
    const portraitPixels: Pixel[] = portraitPixelMap
        .map(toPixel(iToX(nPortaitColumns), iToY(nPortaitColumns, nPortraitRows))(portraitFill))
        .map((p: Pixel) => {
            p.x = offSet(portraitOffset)(p.x)
            p.y = offSet(portraitOffset)(p.y)
            return p
        })
    return mustache.render(template, { borderPixels, portraitPixels })
}
