import crypto from 'crypto'

export const stringToHash = (val: string): Buffer => {
    const hasher = crypto.createHash('sha512')
    hasher.update(val)
    return hasher.digest()
}

