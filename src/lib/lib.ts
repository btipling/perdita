import {
    Observable,
    combineLatest,
    from,
    Subject,
    partition
} from 'rxjs'
import {
    concatMap,
    map,
    tap,
} from 'rxjs/operators'
import {
    Ident,
    Pipeline,
    makePipeline,
    hashPipeline,
    fillConfig,
    writeColor,
    writePixelMap,
    writeSVG,
} from './pipeline'
import {
    notifyOfInvalidEmail,
    notifyOfBuildStep,
} from './notify'
import { writer } from './writer'
import validator from 'validator'


const filterValidEmails = (givenEmails$: Observable<string>) => partition(givenEmails$, (value) => validator.isEmail(value))
const emailSplitter = (email: string): Ident => {
    const parts = email.split('@')
    return { user: parts[0], domain: parts[1] }
}
const pipelineMaker = (destination: string, salt: string) => (ident: Ident): Pipeline => makePipeline({ ident, destination, salt })

const logger = (msg: string) => (x: any) => console.log(msg, x)

export const makeFromEmail = async (emails: string[], destination: string, salt: string): Promise<void> => {
    const givenEmails$ = new Subject<string>()

    const [validEmails$, invalidEmails$] = filterValidEmails(givenEmails$)

    const pipeline$ = validEmails$
        .pipe(
            map(emailSplitter),
            map(pipelineMaker(destination, salt)),
            map(hashPipeline),
            map(fillConfig),
            map(writeColor),
            map(writePixelMap),
            concatMap(writeSVG),
            tap(notifyOfBuildStep),
            tap(logger('output')))

    invalidEmails$.subscribe(notifyOfInvalidEmail)
    pipeline$.subscribe((pipeline: Pipeline) => writer(pipeline.svg, `${pipeline.destination}/${pipeline.ident.user}.${pipeline.ident.domain}.svg`))

    emails.forEach((email: string) => { givenEmails$.next(email) })
}
