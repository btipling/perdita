import { pipe } from 'rxjs'
import { Pipe, pipeline } from 'stream'
import { stringToHash } from '../hash'
import { pixelMapToSVG } from '../view'

export type ViewConfig = {
    hash: Buffer,
    rgb: Buffer,
    data: Buffer,
    colorString: string,
    pixelMap: number[],
}

export type Pipeline = {
    destination: string,
    salt: string,
    ident: Ident,
    portrait: ViewConfig,
    border: ViewConfig,
    svg: string,
}

export type Ident = {
    user: string,
    domain: string,
}

const emptyViewConfig = (): ViewConfig => ({
    hash: Buffer.alloc(0),
    rgb: Buffer.alloc(0),
    data: Buffer.alloc(0),
    colorString: '',
    pixelMap: [],
})

export const makePipeline = ({ salt, destination, ident }: { salt: string, destination: string, ident?: Ident }): Pipeline => ({
    destination,
    salt,
    ident: ident || { user: '', domain: '' },
    portrait: emptyViewConfig(),
    border: emptyViewConfig(),
    svg: '',
})

export const hashPipeline = (pipeline: Pipeline): Pipeline => {
    pipeline.portrait.hash = stringToHash(`${pipeline.salt}-${pipeline.ident.user}`)
    pipeline.border.hash = stringToHash(`${pipeline.salt}-${pipeline.ident.domain}`)
    return pipeline
}

export const fillData = (config: ViewConfig): ViewConfig => {
    config.rgb = config.hash.slice(0, 3)
    config.data = config.hash.slice(3, 28)
    return config
}

export const fillConfig = (pipeline: Pipeline): Pipeline => {
    pipeline.portrait = fillData(pipeline.portrait)
    pipeline.border = fillData(pipeline.border)
    return pipeline
}

const RGBAToColor = (config: ViewConfig): ViewConfig => {
    const r = config.rgb[0]
    const g = config.rgb[1]
    const b = config.rgb[2]
    config.colorString = `#${r.toString(16)}${g.toString(16)}${b.toString(16)}`
    return config
}

export const writeColor = (pipeline: Pipeline): Pipeline => {
    pipeline.portrait = RGBAToColor(pipeline.portrait)
    pipeline.border = RGBAToColor(pipeline.border)
    return pipeline
}

const dataToPixelMap = (config: ViewConfig): ViewConfig => {
    const map: number[] = []
    for (let i = 0; i < config.data.length; i++) {
        map.push(config.data[i] % 2)
    }
    config.pixelMap = map
    return config
}

export const writePixelMap = (pipeline: Pipeline): Pipeline => {
    pipeline.portrait = dataToPixelMap(pipeline.portrait)
    pipeline.border = dataToPixelMap(pipeline.border)
    return pipeline
}

export const writeSVG = async (pipeline: Pipeline): Promise<Pipeline> => {
    console.log('writing pipeline', pipeline.ident)
    pipeline.svg = await pixelMapToSVG(pipeline.portrait.pixelMap, pipeline.portrait.pixelMap, pipeline.border.colorString, pipeline.portrait.colorString)
    return pipeline
}


