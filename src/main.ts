import { Command } from 'commander'
import { makeFromEmail } from './lib/lib'

function main(): void {
    const program = new Command()
    program.version('0.0.1')
    program
        .command('generate')
        .option('-d, --destination <destination>')
        .option('-s, --salt <salt>', 'The salt to use to generate the avatar', 'perrdita')
        .option('-e, --emails [emails...]')
        .description('generate avatars for a bunch of emails')
        .action((cmdObj: Command) => makeFromEmail(cmdObj.emails, cmdObj.destination, cmdObj.salt))
    program.parse(process.argv)
}

main()
